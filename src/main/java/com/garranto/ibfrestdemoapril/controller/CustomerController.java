package com.garranto.ibfrestdemoapril.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/customer")
public class CustomerController {

    @GetMapping(value = "/welcome")
    public String welcomeHandler(){
        return "Welcome to our Customer REST Contoller";
    }
}


//spring mvc

//deplyment descriptor

//jee ---- spring mvc
//http servlet ---  get, post,

//mvc, front controller design pattern

//dispatcher Servlet ----

//Rest Design principles
//Building our rest
