package com.garranto.ibfrestdemoapril.controller;

import com.garranto.ibfrestdemoapril.exceptions.AccountAlreadyPresentException;
import com.garranto.ibfrestdemoapril.exceptions.AccountNotPresentException;
import com.garranto.ibfrestdemoapril.model.Account;
import com.garranto.ibfrestdemoapril.service.AccountService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/accounts")
public class AccountController {


    @Autowired
    private AccountService accountService;

    @GetMapping(value = "/welcome")
    public String welcomeHandler() {
        return "Welcome to our Account REST Contoller";
    }


    @GetMapping(value = "")
    public ResponseEntity<List<Account>> getAllAccountsHandler( @RequestParam(value = "type",required = false) String accountType) {
//        read all the accounts stored from the database
//        do processing if anything required
//        return the response with the processed resource
        List<Account> accountList = accountService.readAllAccounts(accountType);
        ResponseEntity<List<Account>> responseEntity = new ResponseEntity<>(accountList, HttpStatus.OK);
        return responseEntity;
    }

    @PostMapping(value = "")
    public ResponseEntity<String> addNewAccountHandler(@RequestBody Account account) {

        ResponseEntity<String> responseEntity;

        try {
            accountService.addNewAccount(account);
            responseEntity = new ResponseEntity<String>("Account added successfull", HttpStatus.CREATED);
        } catch (AccountAlreadyPresentException e) {
            responseEntity = new ResponseEntity<String>("Account creation Failed. Duplicate Account", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }

    @GetMapping(value = "/{accountNumber}")
    public ResponseEntity<?> getAccountByAccountNumber(@PathVariable("accountNumber") String accountNumber) {

        ResponseEntity<?> responseEntity;

        try {
            Account account = accountService.findAccountByID(accountNumber);
            responseEntity = new ResponseEntity<Account>(account, HttpStatus.OK);
        } catch (AccountNotPresentException e) {
            responseEntity = new ResponseEntity<String>("Account Not Found", HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}

//put -- update a resource
//DELETE


//return response --resource, response headers and status code -- ResponseEntity

//jackson databind
//database----h2
//mysql, oracle, sqlserver, postgre  relation -----  objects
//object relation mismatch impedence ---  object relation mapping -----  hibernate, ibatis
//Data JPA