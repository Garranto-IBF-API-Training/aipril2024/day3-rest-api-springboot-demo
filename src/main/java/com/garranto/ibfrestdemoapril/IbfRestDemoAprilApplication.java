package com.garranto.ibfrestdemoapril;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbfRestDemoAprilApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbfRestDemoAprilApplication.class, args);
	}

}


//endpoints, manipulate using representation, dynamic uri & url paramters

//implement basic crud operations around account
//model
//repositories
//controllers
//service