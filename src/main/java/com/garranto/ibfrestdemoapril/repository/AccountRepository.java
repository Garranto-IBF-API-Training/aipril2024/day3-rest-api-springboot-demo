package com.garranto.ibfrestdemoapril.repository;

import com.garranto.ibfrestdemoapril.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account,String> {
}
