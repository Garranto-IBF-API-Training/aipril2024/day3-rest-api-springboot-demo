package com.garranto.ibfrestdemoapril.service;

import com.garranto.ibfrestdemoapril.exceptions.AccountAlreadyPresentException;
import com.garranto.ibfrestdemoapril.exceptions.AccountNotPresentException;
import com.garranto.ibfrestdemoapril.model.Account;
import com.garranto.ibfrestdemoapril.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;
    public List<Account> readAllAccounts(String accountType){

        if(accountType != null){
            return accountRepository.findAll().stream().filter(x->x.getAccountType().equalsIgnoreCase(accountType)).toList();
        }

            return accountRepository.findAll();
    }

    public boolean addNewAccount(Account account) throws AccountAlreadyPresentException{

        Optional<Account> accountOptional = accountRepository.findById(account.getAccountNumber());
        if(accountOptional.isEmpty()){
            accountRepository.save(account);
            return true;
        }
        throw new AccountAlreadyPresentException();
    }

    public Account findAccountByID(String accountNumber) throws AccountNotPresentException {

        Optional<Account> accountOptional = accountRepository.findById(accountNumber);
        if(accountOptional.isPresent()){

            return accountOptional.get();
        }
        throw new AccountNotPresentException();
    }

//    service method for updation
}
